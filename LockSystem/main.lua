local vehicle

Citizen.CreateThread(function()
	while true do
		Wait(0)

		if(IsControlJustPressed(1, globalConf['CLIENT'].key))then

			vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), false)
			local isPlayerInside = IsPedInAnyVehicle(GetPlayerPed(-1), true)

			local px, py, pz = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
			local coordA = GetEntityCoords(GetPlayerPed(-1), true)

			for i = 1, 200 do
				coordB = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, (6.281)/i, 0.0)
				targetVehicle = GetVehicleInDirection(coordA, coordB)
				if targetVehicle ~= nil and targetVehicle ~= 0 then
					vx, vy, vz = table.unpack(GetEntityCoords(targetVehicle, false))
					if GetDistanceBetweenCoords(px, py, pz, vx, vy, vz, false) then
						distance = GetDistanceBetweenCoords(px, py, pz, vx, vy, vz, false)
						break
					end
				end
			end

			if(not isPlayerInside)then
				if(targetVehicle and targetVehicle ~= 0)then
					vehicle = targetVehicle
					local plate = GetVehicleNumberPlateText(vehicle)
					if(plate)then
						TriggerServerEvent("ls:mainCheck", plate, vehicle, isPlayerInside)
					end
				end
			else
				local plate = GetVehicleNumberPlateText(vehicle)
				if(plate)then
					TriggerServerEvent("ls:mainCheck", plate, vehicle, isPlayerInside)
				end
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Wait(0)
		local ped = GetPlayerPed(-1)
        if DoesEntityExist(GetVehiclePedIsTryingToEnter(PlayerPedId(ped))) then
        	local veh = GetVehiclePedIsTryingToEnter(PlayerPedId(ped))
	        local lock = GetVehicleDoorLockStatus(veh)
	        if lock == 4 then
	        	ClearPedTasks(ped)
	        end
	        if(globalConf['CLIENT'].disableCar_NPC)then
	        	 if lock == 7 then
	                SetVehicleDoorsLocked(veh, 2)
	            end
	            local pedd = GetPedInVehicleSeat(veh, -1)
	            if pedd then
	                SetPedCanBeDraggedOut(pedd, false)
	            end
	        end
        end
	end
end)

local timer = globalConf['CLIENT'].lockTimer * 1000
local time = 0
Citizen.CreateThread(function()
	while true do
		Wait(1000)
		time = time + 1000
	end
end)

RegisterNetEvent('ls:lock')
AddEventHandler('ls:lock', function()
	if(time > timer)then
		if(isLocked(vehicle) == true)then
			SetVehicleDoorsLocked(vehicle, 1)
			SetVehicleDoorsLockedForPlayer(vehicle, PlayerId(), false)
			Notify('Vehicle unlocked')
		else
			SetVehicleDoorsLocked(vehicle, 4) 
			SetVehicleDoorsLockedForPlayer(vehicle, PlayerId(), true)
			Notify('Vehicle locked')
		end
		time = 0
	else
		Notify('You have to wait 3 seconds')
	end
end)

RegisterNetEvent('ls:notify')
AddEventHandler('ls:notify', function(text, duration)
	Notify(text, duration)
end)
