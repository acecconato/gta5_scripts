function GetVehicleInDirection(coordFrom, coordTo)
	local rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed(-1), 0)
	local a, b, c, d, vehicle = GetRaycastResult(rayHandle)
	return vehicle
end

function Notify(text, duration)
	if(globalConf['CLIENT'].notification)then
		if(globalConf['CLIENT'].notification == 1)then
			if(not duration)then
				duration = 0.080
			end
			SetNotificationTextEntry("STRING")
			AddTextComponentString(text)
			Citizen.InvokeNative(0x1E6611149DB3DB6B, "CHAR_LIFEINVADER", "CHAR_LIFEINVADER", true, 1, "LockSystem", "Version " .. _VERSION, duration)
			DrawNotification_4(false, true)
		elseif(globalConf['CLIENT'].notification == 2)then
			TriggerEvent('chatMessage', '^1LockSystem', {255, 255, 255}, text)
		else
			return
		end
	else
		return
	end
end

function isLocked(vehicle)
	local status = GetVehicleDoorLockStatus(vehicle)
	if(status == 0 or status == 1)then
		return false
	else
		return true
	end
end