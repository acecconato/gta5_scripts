# --- **Configuration** ---

## Weapons with which can steal a player :

Go to `function.lua` line 2 and check for :
*local weaponStealeableList = {453432689, 1593441988, 584646201, 2578377531, 324215364}*

Add the hash of the weapon you wish (list here : https://consolebang.com/threads/weapon-and-explosion-hashes-list-gta-v.29092/ )

### Objets you can steal (or not)

Go to your database -> `items` column. You have a new column named `can_steal`.
1 = Can be stolen / 0 = Can't be stolen

---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

# Only ESX developers have permission to edit and share it.